﻿using System;
using System.IO;
namespace FileOperation
{
    class FileOperation
    {
        static void Main(string[] args)
        {
            string file = @"text.txt";
            int count = 0;
            bool enter = true;
            while (enter)
            {
                try
                {
                    Console.Write("\n");
                    Console.Write("WELCOME \n");
                    Console.WriteLine("1 -----Add the text in the file");
                    Console.WriteLine("2 -----Append the line");
                    Console.WriteLine("3 -----Read the text printed");
                    Console.WriteLine("4 -----Count the number of lines and display last line");
                    Console.WriteLine("5 -----Delete the file created");
                    Console.WriteLine("6 ----- Exit File");
                    Console.Write("\n");
                    Console.WriteLine("Please Enter your choice: ");
                    int choice;
                    choice = Convert.ToInt32(Console.ReadLine());
                    switch (choice)
                    {
                        case 1:
                            AddText();
                            break;
                        case 2:
                            AppendText();
                            break;
                        case 3:
                            ReadText();
                            break;
                        case 4:
                            CountLines();
                            break;
                        case 5:
                            DeleteFile();
                            break;
                        case 6:
                            ExitFile();
                            break;
                        default:
                            Console.WriteLine("INVALID CHOICE");
                            break;
                    }
                }
                catch (FormatException error)
                {
                    Console.WriteLine(error.Message);
                }

                void AddText()
                {
                    try
                    {
                        Console.WriteLine("number of lines to be added to the file = ");
                        int numberOfLine = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Enter the Contents to be added in the file : ");
                        FileStream fileWrite = new FileStream(file, FileMode.Create);
                        using (StreamWriter writer = new StreamWriter(fileWrite))
                        {
                            for (int i = 0; i < numberOfLine; i++)
                            {
                                string addText = Console.ReadLine();
                                writer.WriteLine(addText);
                            }
                        }
                        fileWrite.Close();
                    }
                    catch (FormatException error)
                    {
                        Console.WriteLine(error.Message);
                    }
                    catch (IOException error)
                    {
                        Console.WriteLine(error.Message);
                    }
                }

                void AppendText()
                {
                    try
                    {
                        Console.WriteLine("number of lines to be appended = ");
                        int number = Convert.ToInt32(Console.ReadLine());
                        using (StreamWriter streamWriter = File.AppendText(file))
                        {
                            Console.WriteLine("\nEnter the text : ");
                            for (int i = 0; i < number; i++)
                            {
                                string appendText = Console.ReadLine();
                                streamWriter.WriteLine(appendText);
                            }
                            streamWriter.Close();
                            Console.WriteLine("\nContents of the file :");
                            Console.WriteLine(File.ReadAllText(file));
                        }
                    }
                    catch (FormatException error)
                    {
                        Console.WriteLine(error.Message);
                    }
                }

                void ReadText()
                {
                    try
                    {
                        Console.WriteLine("\nContents of the file:");
                        Console.WriteLine(File.ReadAllText(file));
                    }
                    catch (IOException error)
                    {
                        Console.WriteLine(error.Message);
                    }
                }

                void CountLines()
                {
                    try
                    {
                        string[] text = File.ReadAllLines(file);
                        using (StreamReader read = File.OpenText(file))
                        {
                            while ((read.ReadLine()) != null)
                            {
                                count++;
                            }
                            Console.WriteLine("Total number of lines = " + count);
                            Console.WriteLine("\nThe last line is " + text[count - 1]);
                        }
                    }
                    catch (IOException error)
                    {
                        Console.WriteLine(error.Message);
                    }

                }

                void DeleteFile()
                {
                    try
                    {
                        File.Delete(file);
                        Console.WriteLine("Deleted Successfully");
                    }
                    catch (IOException error)
                    {
                        Console.WriteLine(error.Message);
                    }
                }

                void ExitFile()
                {
                    try
                    {
                        Environment.Exit(0);
                        Console.WriteLine("File Exited successfully");
                    }
                    catch (IOException error)
                    {
                        Console.WriteLine(error.Message);
                    }
                }
            }
        }
    }
}