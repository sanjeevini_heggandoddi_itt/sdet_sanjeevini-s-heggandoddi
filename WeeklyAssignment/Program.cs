﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LibraryApplication
{
    interface IBooks
    {
        void AddBook();
        void SearchBook();
        void UpdateBook();
        void DeleteBook();
        void AvailableBook();

    }

    class Book : IBooks
    {
        public virtual void AddBook()
        {
            Console.WriteLine("Adding the book ");
        }

        public virtual void SearchBook()
        {
            Console.WriteLine("Searching the book ");
        }

        public virtual void DeleteBook()
        {
            Console.WriteLine("Deleting the book ");
        }

        public virtual void UpdateBook()
        {
            Console.WriteLine("Updating the book ");
        }

        public virtual void AvailableBook()
        {
            Console.WriteLine("List of available book ");
        }
    }

    class NoteBook : Book
    {
        private int _bookID;
        private string _bookTitle;
        private string _bookType;
        static List<NoteBook> bookList = new List<NoteBook>();
        static NoteBook bookObject = new NoteBook();

        public override void AddBook()
        {
            NoteBook bookObject = new NoteBook();
            Console.WriteLine("Book ID :- {0}", bookObject._bookID = bookList.Count + 1);
            bookObject._bookID = Convert.ToInt32(bookObject._bookID);
            Console.WriteLine("Book Name:-");
            bookObject._bookTitle = Console.ReadLine();
            Console.WriteLine("Book Type:-Horror;Fictional;Adventures");
            bookObject._bookType = Console.ReadLine();
            bookList.Add(bookObject);
        }

        public override void SearchBook()
        {
            if (bookList.Count() == 0)
            {
                Console.WriteLine("Empty\nAdd the books");
            }
            else
            {
                Console.WriteLine("Enter the Book ID to be searched:");
                Int32 id = Convert.ToInt32(Console.ReadLine());
                for (int i = 0; i < bookList.Count; i++)
                {
                    if (bookList[i]._bookID == id)
                    {
                        Console.WriteLine("Book name:-{0}\n" + "Book Type:-{1}\n", bookList[i]._bookTitle, bookList[i]._bookType);
                    }
                }
            }
        }

        public override void DeleteBook()
        {
            if (bookList.Count() == 0)
            {
                Console.WriteLine("Empty\nAdd the books ");
            }
            else
            {
                Console.WriteLine("Enter the Book ID to be deleted:");
                Int32 Delete = Convert.ToInt32(Console.ReadLine());
                for (int i = 0; i < bookList.Count; i++)
                {
                    if (Delete == bookList[i]._bookID)
                        bookList.RemoveAt(i);
                }
                Console.WriteLine("The book you entered is deleted\n", Delete);
            }
            bookList.Add(bookObject);
        }

        public override void UpdateBook()
        {
            if (bookList.Count() == 0)
            {
                Console.WriteLine("Empty\nAdd the books\n");
            }
            else
            {
                int search;
                Book bookObject = new Book();
                Console.WriteLine("Enter the ID of the Book to be updated: ");
                search = Convert.ToInt32(Console.ReadLine());
                for (int i = 0; i < bookList.Count; i++)
                {
                    if (bookList[i]._bookID == search)
                    {
                        Console.WriteLine("Enter the updated name:");
                        bookList[i]._bookTitle = Console.ReadLine();
                    }
                }
            }
        }

        public override void AvailableBook()
        {
            if (bookList.Count == 0)
            {
                Console.WriteLine("empty\nAdd the books\n");
            }
            else
            {
                foreach (NoteBook bookObject in bookList)
                {
                    Console.WriteLine("Book Name: {0}\n" + "Book ID: {1}\n" + "Book Type: {2}\n", bookObject._bookTitle, bookObject._bookID, bookObject._bookType);
                }
            }
        }

        public static void Main(string[] args)
        {

            Console.Title = "WELCOME TO THE LIBRARY";
            bool enter = true;
            while (enter)
            {
                Console.WriteLine("\n");
                Console.WriteLine("WELCOME\n");
                Console.WriteLine("1----Add the book in the list");
                Console.WriteLine("2----Search the book");
                Console.WriteLine("3----Update book");
                Console.WriteLine("4----List the available list");
                Console.WriteLine("5----Delete the book from list");
                Console.WriteLine("6----Exit\n" + "Choose the task: ");
                Int32? value;
                value = Convert.ToInt32(Console.ReadLine());

                switch (value)
                {
                    case 1:
                        bookObject.AddBook();
                        break;
                    case 2:
                        bookObject.SearchBook();
                        break;
                    case 3:
                        bookObject.UpdateBook();
                        break;
                    case 4:
                        bookObject.AvailableBook();
                        break;
                    case 5:
                        bookObject.DeleteBook();
                        break;
                    case 6:
                        enter = false;
                        break;
                    default:
                        Console.WriteLine("Invalid Choice");
                        break;
                }
            }
        }
    }
}