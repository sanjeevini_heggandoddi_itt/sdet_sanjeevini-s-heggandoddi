using System;
namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("CALCULATOR");
            Console.WriteLine("1.+");
            Console.WriteLine("2.-");
            Console.WriteLine("3.*");
            Console.WriteLine("4./");
            Console.WriteLine("Enter the choice:");
            int choice = Convert.ToInt32(Console.ReadLine());
            int a,b,c;
            switch(choice)
            {
                case 1:
                    Console.WriteLine("Enter num1:");
                    a=Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Enter num2:");
                    b =Convert.ToInt32(Console.ReadLine());
                    c = a + b;
                    Console.WriteLine("sum= {0}", c);
                    break;
                case 2:
                    Console.Write("Enter num1:");
                    a = Convert.ToInt32(Console.ReadLine());
                    Console.Write("Enter num2:");
                    b = Convert.ToInt32(Console.ReadLine());
                    c = a - b;
                    Console.WriteLine("Difference = {0}", c);
                    break;
                case 3:
                    Console.Write("Enter num1:");
                    a = Convert.ToInt32(Console.ReadLine());
                    Console.Write("Enter num2:");
                    b = Convert.ToInt32(Console.ReadLine());
                    c = a * b;
                    Console.WriteLine("Product = {0}", c);
                    break;
                case 4:
                    Console.Write("Enter num1:");
                    a = Convert.ToInt32(Console.ReadLine());
                    Console.Write("Enter num2:");
                    b = Convert.ToInt32(Console.ReadLine());
                    c = a / b;
                    Console.WriteLine("Quotient = {0}", c);
                    break;
                default:
                    Console.WriteLine("Invalid Choice");
                    break;
            }
            Console.ReadKey();
        }
    }
}