﻿using System;
using System.Collections;
namespace QueueNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            Queue Q=new Queue();
            Q.Enqueue("One");
            Q.Enqueue("Two");
            Q.Enqueue("Three");
            Q.Enqueue("Four");
            Q.Enqueue("Five");
            foreach (Object obj in Q)
            {
                Console.WriteLine(obj + " ");
            }
            Console.WriteLine();
                Console.WriteLine(Q.Dequeue());
            Console.WriteLine();
            foreach (object obj in Q )
            {
                Console.Write(obj + " ");
            }
            Console.WriteLine();
            Console.WriteLine(Q.Peek());
            Console.WriteLine();
            foreach (object obj in Q)
            {
                Console.Write(obj + " ");
            }
            Console.Write("Total number of elements in the Queue are : ");

            Console.WriteLine(Q.Count);
            Console.ReadKey();
        }
    }
}
