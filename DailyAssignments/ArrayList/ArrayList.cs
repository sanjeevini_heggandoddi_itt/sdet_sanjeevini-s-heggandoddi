﻿using System;
using System.Collections;
namespace ArrayListCollection
{
    class program
    {
        static void Main(string[] args)
        {
            ArrayList al=new ArrayList();
            Console.WriteLine("Intial capacity:" + al.Capacity);
            al.Add(100);al.Add(200);al.Add(300);al.Add(400);
            Console.WriteLine("Capacity after addingq:" + al.Capacity);
            al.Add(500);
            Console.WriteLine("Capacity after adding 5th element:" + al.Capacity);
            foreach(object o in al)
                Console.WriteLine(o + " ");
            al.Insert(3, 350);
            foreach (object o in al)
                Console.WriteLine(o + " ");
            al.RemoveAt(1);
            foreach (object o in al)
                Console.WriteLine(o + " ");
        }
    }
}