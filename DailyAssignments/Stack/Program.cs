﻿using System;
using System.Collections;
namespace StackExample
{
    class Program
    {
        static void Main(string[] args)
        {
            Stack s=new Stack();
            s.Push(6);
            s.Push("May");
            s.Push(2000);
            s.Push("Saturday");
            foreach (object obj in s)//Printing the stack items using foreach loop
            {
                Console.Write(obj + " ");
            }
            Console.WriteLine();
            Console.WriteLine(s.Pop());//Deleting the last item added
            Console.WriteLine();
            foreach (object obj in s)//Printing the stack items using foreach loop
            {
                Console.Write(obj + " ");
            }
            Console.WriteLine();
            Console.WriteLine(s.Peek());
            Console.WriteLine();
            foreach (object obj in s)//Printing the items after Peek method
            {
                Console.Write(obj + " ");
            }
            Console.ReadKey();
        }
    }
}



    
