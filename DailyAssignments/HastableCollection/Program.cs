﻿using System;
using System.Collections;
namespace PersonDetails
{
    class Program
    {
         static void Main(string[] args)
        {
            Hashtable ht = new Hashtable();
            ht.Add("Name", "Sanjana");
            ht.Add("Job", "Teacher");
            ht.Add("PhoneNumber", "999001000");
            ht.Add("Location", "Dharwad");
           Console.WriteLine("The values of Hashtable ht are: ");
           foreach (object o in ht.Values)
                Console.WriteLine(o);
            Console.WriteLine("The keys of Hashtable ht are: ");
            foreach (object o in ht.Keys)
                Console.WriteLine(o);
            Console.ReadLine();
        }
    }
}

